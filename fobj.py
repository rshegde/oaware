# the objective functions etc, will need to change for each problem
# these are for multilayered thin film problem


import multiprocessing as mp
import oldqx as oqx
import numpy as np 



def create_ref_R( l_low, l_high, l_pts,  l_left, l_right):
    #default 0.0, then create a high ref window 
    
    R_ref = np.zeros(l_pts)
    lam_inv = np.linspace(1/l_low, 1/l_high, num=l_pts, endpoint=True)
    lams = 1.0/lam_inv
    if l_left <= 800.0:
        R_ref[ np.logical_and(lams >= l_left, lams <= l_right)] = 1.0
    
    return R_ref

def arc_par_gp(modelg, pop, x_low, x_hig):
    x_in = 2.0*(pop - x_low)/(x_hig - x_low) - 1.0
    y_in = modelg.predict(x_in)
    lam_left = 900.0
    lam_right = 932.0
    
    # implement various functionalities here
    R_ref = create_ref_R(l_low = 400, l_high=800, l_pts = y_in[0].shape[0],  l_left = lam_left, l_right = lam_right)
    R_ref_tile = np.tile(R_ref, (pop.shape[0], 1))
    
    subtr = (y_in - R_ref_tile)**2
    return 100*np.mean(y_in, axis=1)


def tmm_wrapper3(arg):
    args, kwargs = arg
    return oqx.tmm_eval_wsweep(*args, **kwargs)


def arc_par(pop, **kwargs):
    jobs = []
    pool=mp.Pool(100)
    D_low = 10
    D_up = 100
    for indiv in pop:
        #indiv = indiv.reshape((int(indiv.size/2), 2))
        #indiv[:,1] = mkq.digitize_qx(indiv[:,1], dlevels=2)
        #indiv = rndtop5(indiv)
        #indiv = indiv.flatten()
        jobs.append((rndtop5(D_low + (D_up- D_low)*indiv), 0))
        #jobs.append((indiv, 0))
    arg = [(j, kwargs) for j in jobs]
    answ = np.array(pool.map(tmm_wrapper3, arg))
    pool.close()
    return answ

def rndtop5(x):
    return np.round(x*2.0)/2

def tmm_wrapper2(arg):
    args, kwargs = arg
    lam_left = 900.0
    lam_right = 932.0
    Rs = oqx.tmm_eval_wsweep(*args, **kwargs) 
    
# #     implement various functionalities here
    R_ref = np.zeros(Rs.shape[0])
    lam_inv = np.linspace(1/400.0, 1/800.0, num=Rs.shape[0], endpoint=True)
    lams = 1.0/lam_inv
    R_ref[ np.logical_and(lams >=lam_left, lams <= lam_right)] = 1.0
    subtr = (Rs - R_ref)**2

    return 100*np.mean(np.sqrt(subtr))
    #return 100*np.mean(Rs) 

def calc_fits(pop, **kwargs):
    jobs = []
    pool=mp.Pool(90)
    D_low = 10
    D_up = 100
    for indiv in pop:
        jobs.append((rndtop5(D_low + (D_up- D_low)*indiv), 0))
    arg = [(j, kwargs) for j in jobs]
    answ = np.array(pool.map(tmm_wrapper2, arg))
    pool.close()
    return answ 

def calc_acfits(pop, **kwargs):
    jobs = []
    pool=mp.Pool(90)
    D_low = 10
    D_up = 100
    for indiv in pop:
        jobs.append((rndtop5(D_up*indiv), 0))
    arg = [(j, kwargs) for j in jobs]
    answ = np.array(pool.map(tmm_wrapper2, arg))
    pool.close()
    return answ 




