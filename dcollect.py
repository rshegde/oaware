import de_online as deo
import fobj as fo
import diversipy as hycu
import scnets as scn
import de_runner as runde
import time
import numpy as np

#!/usr/bin/python

import sys

def main():
    # print command line arguments
    xfname = sys.argv[1]  #xfilname
    yfname = sys.argv[2]  #yfilname
    P = int(sys.argv[3])  #popsize
    num_reps = int(sys.argv[4])  #popsize

    #!rm -rf *.dat

    num_isles = 1
    num_layers_i = 16 
    num_gens  = 1
    popsize = P

    start_time= time.time()
    pulist = runde.lhs_initializer(num_isles = num_isles, D = num_layers_i, num_gens  = num_gens, popsize = popsize)


    evalcnt = 0
    #x_train, x_test, y_train, y_test = train_test_split(X[:Sz], Y[:Sz], test_size=0.2)
    bids, bests, bfits, hstry_ac,nfunc=deo.de_plain(
            fobj=fo.calc_fits,  # objective function, serial eval
            fsec=fo.arc_par,
            xfname=xfname,
            yfname=yfname,
            num_layers_i=num_layers_i,
            num_isles = num_isles, #number of islands
            num_gens =num_gens,
            poplist=pulist,  # already initialized population
            mut=0.4,  # mutation rate
            crossp=0.4,  # crossover
            lenp=0.0,
            lins = 0.0,
            popsize=popsize,  # the population size
            its=32, # the number of iterations needed to run for
            verbose=1,
            lam_low=400, 
            lam_high=800, 
            lam_pts=64)
    evalcnt+=nfunc

    print("time:" + str((time.time() - start_time)/60.0))
    

    x_train = np.loadtxt(xfname+".dat")
    y_train = np.loadtxt(yfname+".dat")
    xt = np.split(x_train, num_isles)    
    yt = np.split(y_train, num_isles)    
    for isl in range(num_isles):
        namef = xfname+str(isl)+".dat"
        rawxi = open(namef, "a") # or "wb"
        np.savetxt(rawxi, xt[isl])
        rawxi.close()
        namef = yfname+str(isl)+".dat"
        rawyi = open(namef, "a") # or "wb"
        np.savetxt(rawyi, yt[isl])
        rawyi.close()



    for ind in range(num_reps):    
        modelsi = runde.run_onde(num_isles, xfname, yfname)
        print("time:" + str((time.time() - start_time)/60.0))
        bids, bests, bfits, hstry_ac,nfunc, errlist = deo.de_isl_online(
            fobj=fo.calc_fits,  # objective function, serial eval
            fobj_mx=fo.arc_par_gp,
            fsec=fo.arc_par,
            xfname=xfname,
            yfname=yfname,
            mx_model=modelsi,
            num_layers_i=num_layers_i,
            num_isles = num_isles, #number of islands
            num_gens =num_gens,
            poplist=pulist,  # already initialized population
            mut=0.4,  # mutation rate
            crossp=0.4,  # crossover
            lenp=0.0,
            lins = 0.0,
            popsize=popsize,  # the population size
            its=32, # the number of iterations needed to run for
            verbose=1,
            lam_low=400, 
            lam_high=800, 
            lam_pts=64)
        print("time:" + str((time.time() - start_time)/60.0))
    # #print(errlist)
        evalcnt += nfunc


    # # #     #print(rndtop5(d_min + (d_max - d_min)*bests[np.argmin(bfits)]))
    # # #     #print(np.sum(rndtop5(d_min + (d_max - d_min)*bests[np.argmin(bfits)])))
    print(bfits, evalcnt)

if __name__ == "__main__":
    main()