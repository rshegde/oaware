# this python file will run multiple des and collate results


import de_online as deo
import fobj as fo
import diversipy as hycu
import scnets as scn
import numpy as np
import multiprocessing as mp
import copy as cp


def lhs_initializer(    num_isles = 1, D = 16, num_gens  = 1, popsize = 80, final =[]):
    init_seed = 0
    init_seed_e = int(init_seed/num_isles)
    rand_gens = num_isles*popsize - init_seed
    rand_gens_e = rand_gens/num_isles
    strat = hycu.stratify_generalized(dimension=D, num_strata=rand_gens)
    pts = hycu.stratified_sampling(strat)
    item = 0
    for sol in final:
        pts[int(item/4)*popsize + item%4] = sol
        item+=1
    spl1 = np.split(pts, num_isles)

    pulist = []
    for isl in range(num_isles):
        pulist.append( [x.flatten() for x in  np.split(spl1[isl], popsize)] )
    return pulist
    

def run_onde(N, xfname, yfname):
    
    print("Training surrogates now ...")
    models_f = ["models" + str(isle) for isle in range(N)]
    
    for i in range(N):
        namef = xfname+str(i)+".dat"
        xt = np.loadtxt(namef)
        namef = yfname+str(i)+".dat"
        yt = np.loadtxt(namef)
        
        models_f[i] = scn.fullycon(in_size=16, 
                         out_size=64, 
                         batch_size=64,
                         N_hidden=3, 
                         N_neurons=128, 
                         N_gpus=1)
        history3 = models_f[i].fit(xt, yt,
                        batch_size=64,
                        epochs=100, 
                        verbose=0)
        #models_f.append(   cp.deepcopy(model_h) )
        
    return models_f
    
    










    