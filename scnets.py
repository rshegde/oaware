from keras import backend as K
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout
from keras.layers import Reshape, UpSampling1D, Conv1D
from keras.layers import Flatten, Activation
from keras.utils import np_utils, multi_gpu_model
from keras.regularizers import l2
from keras.wrappers.scikit_learn import KerasRegressor
from keras.optimizers import Adam
import numpy as np

from keras.layers import PReLU
from keras.models import Model
from keras.layers import Input, Add
from keras.layers.normalization import BatchNormalization
from keras.layers import PReLU
#from keras.utils import to_channels_first
from keras.callbacks import ReduceLROnPlateau
from keras.utils.generic_utils import get_custom_objects
from keras.regularizers import l1_l2

import tensorflow as tf

def swish(x):
    return (K.sigmoid(x) * x)

#naive percentage loss
def relerr_loss2(y_true, y_pred):
    y_err = K.abs(y_true - y_pred)
    y_err_f = K.mean(y_err**2, axis = 0)
    
    #print(y_err.shape)
    return K.mean(y_err_f)




# Loss functtion
def ssim_loss(y_true, y_pred):
    
    alpha = 0.5  # this mixes with mae
    
    y_err = K.abs(y_true - y_pred)
    y_err_f = K.mean(y_err**2, axis = 0)
    y_true = tf.expand_dims(y_true,2)
    y_true = tf.expand_dims(y_true,3)

    
    y_pred = tf.expand_dims(y_pred,2)
    y_pred = tf.expand_dims(y_pred,3)
    
    kernel_sz = 15
    k1=0.01
    k2=0.03 
    max_value=1.0
    c1 = (k1 * max_value) ** 2
    c2 = (k2 * max_value) ** 2

    patches_true = tf.extract_image_patches(images=y_true,
                           ksizes=[1, kernel_sz, 1, 1],
                           strides=[1, 1, 1, 1],
                           rates=[1, 1, 1, 1],
                           padding='VALID')
    u_true = K.mean(patches_true, axis=-1)
    var_true = K.var(patches_true, axis=-1)

    patches_pred = tf.extract_image_patches(images=y_pred,
                           ksizes=[1, kernel_sz, 1, 1],
                           strides=[1, 1, 1, 1],
                           rates=[1, 1, 1, 1],
                           padding='VALID')
    u_pred = K.mean(patches_pred, axis=-1)
    var_pred = K.var(patches_pred, axis=-1)
    covar_true_pred = K.mean(patches_true * patches_pred, axis=-1) - u_true * u_pred

    ssimm = ( tf.add(2 * u_true * u_pred, c1)) * (tf.add(2 * covar_true_pred, c2))
    denom = ((K.square(u_true)+ K.square(u_pred) +c1) * (var_pred + var_true + c2))
    ssimm = tf.add(1.0, -1*ssimm/denom)

     
    y_err = K.abs(y_true - y_pred)
    y_err_f = K.mean(y_err**2, axis = 0)
    
    
    return alpha*K.mean(ssimm, axis=1) + (1.0 - alpha)*K.mean(y_err_f)


# def ssim_loss(y_true, y_pred):
#     k1=0.01
#     k2=0.03 
#     kernel_size=3 
#     max_value=1.0
#     c1 = (k1 * max_value) ** 2
#     c2 = (k2 * max_value) ** 2
    
#     #y_n = y_true[:,
#     dim_ordering = K.image_data_format()
    
#     kernel = [3, 1]
#     y_true = K.reshape(y_true, [-1] + list(K.int_shape(y_pred)[1:]))
#     y_pred = K.reshape(y_pred, [-1] + list(K.int_shape(y_pred)[1:]))

#     patches_pred = KC.extract_image_patches(y_pred, kernel, kernel, 'valid',
#                                            K.image_data_format())
# #     patches_true = K.extract_image_patches(y_true, kernel, kernel, 'valid',
# #                                             K.image_data_format())
# #     # Reshape to get the var in the cells
# #     bs, w, h, c1, c2, c3 = K.shape(patches_pred)
# #     patches_pred = K.reshape(patches_pred, [-1, w, h, c1 * c2 * c3])
# #     patches_true = K.reshape(patches_true, [-1, w, h, c1 * c2 * c3])
# #     # Get mean
# #     u_true = K.mean(patches_true, axis=-1)
# #     u_pred = K.mean(patches_pred, axis=-1)
# #     # Get variance
# #     var_true = K.var(patches_true, axis=-1)
# #     var_pred = K.var(patches_pred, axis=-1)
# #     # Get std dev
# #     covar_true_pred = K.mean(patches_true * patches_pred, axis=-1) - u_true * u_pred
# #     ssim = (2 * u_true * u_pred + c1) * (2 * covar_true_pred + c2)
# #     denom = ((K.square(u_true)+ K.square(u_pred) +c1) * (var_pred + var_true + c2))
# #     ssim /= denom  # no need for clipping, c1 and c2 make the denom non-zero
    
#     y_err = K.abs(y_true - y_pred)
#     y_err_f = K.mean(y_err**2, axis = 0)
    
#     #print(y_err.shape)
#     return K.mean(y_err_f)
    
    




#function to test performance on testset
def calc_mre_K(y_true, y_pred):
    y_err = 100*K.abs(y_true[:,:-1] - y_pred[:,:-1])
    return K.max(y_err)

#function to test performance on testset
def calc_mre_K2(y_true, y_pred):
    y_err = 100*K.abs(y_true[:,:] - y_pred[:,:])
    return K.max(y_err)



def fullycon( in_size=8, 
             out_size=256, 
             batch_size=32,
             N_hidden=3, 
             N_neurons=256, 
             N_gpus=1):
    """
    Returns a fully-connected model which will take a normalized size vector and return a
    spectrum
    in_size: length of the size vector
    out_size: length of the spectrum vector
    N_hidden: number of hidden layers
    N_neurons: number of neurons in each of the hidden layers
    """
    model = Sequential()
    activat_f = 'swish'
    model.add(Dense(N_neurons, input_dim=in_size, 
                        kernel_initializer='normal', kernel_regularizer=l1_l2(l1=0, l2=1e-3),
                    name='first' ))
    get_custom_objects().update({'swish': Activation(swish)})
    model.add(Activation(activat_f))
    #model.add(Activation('relu'))
    
    for h in np.arange(N_hidden):
        lname = "H"+str(h)
        model.add(Dense(units=N_neurons, 
                        kernel_initializer='normal', kernel_regularizer=l1_l2(l1=0, l2=1e-3), activation=activat_f, name=lname ))
        #model.add(Activation('swish'))

   

    model.add(Dense(units = out_size, kernel_initializer='normal', kernel_regularizer=l1_l2(l1=0, l2=1e-3), activation='sigmoid', name='last'))
    #model.add(Activation('sigmoid'))

    # Compile model
    if N_gpus == 1:
        model.compile(loss=ssim_loss, optimizer='adam', metrics=[calc_mre_K])
    else:
        gpu_list = ["gpu(%d)" % i for i in range(N_gpus)]
        model.compile(loss=ssim_loss, optimizer='adam', metrics=[calc_mre_K2], context = gpu_list)
    return model


def fullycon_mse( in_size=8, 
             out_size=256, 
             batch_size=32,
             N_hidden=3, 
             N_neurons=256, 
             N_gpus=1):
    """
    Returns a fully-connected model which will take a normalized size vector and return a
    spectrum
    in_size: length of the size vector
    out_size: length of the spectrum vector
    N_hidden: number of hidden layers
    N_neurons: number of neurons in each of the hidden layers
    """
    model = Sequential()
    #activat_f = 'swish'
    model.add(Dense(N_neurons, input_dim=in_size, 
                        kernel_initializer='normal', kernel_regularizer=l1_l2(l1=0, l2=1e-4),
                    name='first' ))
    #get_custom_objects().update({'swish': Activation(swish)})
    #model.add(Activation(activat_f))
    model.add(Activation('relu'))
    
    for h in np.arange(N_hidden):
        lname = "H"+str(h)
        model.add(Dense(units=N_neurons, 
                        kernel_initializer='normal', kernel_regularizer=l1_l2(l1=0, l2=1e-4), activation='relu', name=lname ))
        #model.add(Activation('swish'))

   

    model.add(Dense(units = out_size, kernel_initializer='normal', kernel_regularizer=l1_l2(l1=0, l2=1e-4), activation='sigmoid', name='last'))
    #model.add(Activation('sigmoid'))

    # Compile model
    if N_gpus == 1:
        model.compile(loss=relerr_loss2, optimizer='adam', metrics=[calc_mre_K])
    else:
        gpu_list = ["gpu(%d)" % i for i in range(N_gpus)]
        model.compile(loss=relerr_loss2, optimizer='adam', metrics=[calc_mre_K2], context = gpu_list)
    return model



