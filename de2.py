import numpy as np
import random
import mxnet.ndarray as nd
import mxnet as mx

from itertools import chain




#test for exact equality
def arreq_in_list(myarr, list_arrays):
    return next((True for elem in list_arrays if np.array_equal(elem, myarr)), False)


def lengthen_best(best):
    return np.append(best, np.random.rand())

def ins_best(best):
    ins_loc = np.random.choice(best.size)
    return np.insert(best, (ins_loc, ins_loc), np.random.uniform(0,1,2))

def pad_chrom(inp, max_len):
    if inp.size < max_len:
        #ins_loc = np.random.choice(inp.size, max_len - inp.size, replace=False)
        
        ins_val = np.zeros(max_len - inp.size)
        #ins_val = np.random.uniform(0,1, max_len - inp.size)
        return np.append(inp, ins_val)
    else:
        return inp


def cre_mutant(best, b, c, mut):
#     max_len = max( best.size, b.size, c.size)
#     best_n = pad_chrom(best, max_len=max_len)
#     b_n = pad_chrom(b, max_len=max_len)
#     c_n = pad_chrom(c, max_len=max_len)
    #return np.clip(best_n + mut*(b_n - c_n), 0, 1)
    return np.clip(best + mut*(b - c), 0, 1)



def crossov(mutant, curr, crossp):
#     max_len = max( curr.size, mutant.size)
#     if curr.size == mutant.size:
#         c_r = curr
#         m_r = mutant
#     else:
#         c_r = pad_chrom(curr, max_len)
#         m_r = pad_chrom(mutant, max_len)
    
    cross_points = np.random.rand(curr.size) > crossp
#     if not np.any(cross_points):
#         cross_points[np.random.randint(0, curr.size)] = True
    #return np.where(cross_points, m_r, c_r)
    return np.where(cross_points, mutant, curr)


def de_isl_sur(
    fobj,  # objective function, serial eval
    fobj_mx,
    fsec, 
    mx_model=[], 
    hum_model=[],
    x_l= 0.1,
    x_h = 1.0,
    num_layers_i=16,
    num_isles = 5, #number of islands
    num_gens =5,
    poplist=[],  # already initialized population
    mut=0.8,  # mutation rate
    crossp=0.7,  # crossover
    lenp=0.08,
    lins=0.06,
    popsize=20,  # the population size
    its=1000, # the number of iterations needed to run for
    verbose=0,
    **kwargs
):
    """
        This function performs diff evolution using fobj evaluated in parallel  
        returns the  last pop, fitness of pop, best individual and opt history 
    """
    bucketsz = 0 
    history = []
    errhist =[]
    #mut = 0.8 + 0.6*np.random.random()
    bids = np.zeros(num_isles, dtype=int)
    bfits = np.zeros(num_isles)
    #mut_r = np.random.uniform(0.8, 0.8, num_isles)
    #cro_r = np.random.uniform(1.0, 1.0, num_isles)
    num_func_evals = 0
    use_gpu = 1
    ls_fac = 10 #this is the local search factor
    shft = np.transpose(popsize*np.tile(np.arange(num_isles), (popsize,1)))
    shifts = np.tile(np.array(shft.flatten(), dtype='int32'), (ls_fac, ))
    
#     generate the best/abc, mut and crossp vectors
    bora = np.reshape(np.tile(np.random.uniform(0.6, 1.0, (ls_fac,1)), (1,num_isles*popsize)), (ls_fac*num_isles*popsize,))
    mutrad = np.reshape(np.tile(np.random.uniform(0.4, 0.4, (ls_fac,1)), (1,num_layers_i*num_isles*popsize)), (ls_fac*num_isles*popsize,num_layers_i))
    crp =    np.reshape(np.tile(np.random.uniform(0.4, 0.4, (ls_fac,1)), (1,num_layers_i*num_isles*popsize)), (ls_fac*num_isles*popsize,num_layers_i))
    #print(mutrad)
    
    trilist = []
    for isl in range(num_isles):
        tmp = np.random.uniform(0,1, popsize*num_layers_i)
        trilist.append( np.split(tmp, popsize) ) 
    tmp2 = np.random.uniform(0,1, num_isles*num_layers_i)
    bests = np.split(tmp2, num_isles)

    for gen in range(num_gens):
        if verbose == 0:
            print("==============================")
            print("Epoch #:" + str(gen + 1))
            print("==============================")
        
        isl = list(chain.from_iterable(poplist))
        fitness_isl = np.asarray(fobj(isl, **kwargs))
        
        num_func_evals+=len(isl)
        fitness = np.split(fitness_isl, num_isles)
        
        for isln in range(num_isles):
            bids[isln] = np.argmin(fitness[isln])
            bests[isln] = poplist[isln][bids[isln]]
            
        for i in range(its):
            isl = list(chain.from_iterable(poplist))
            poplist_gpu = np.array(isl) 
            fitness_isl_gpu = fobj_mx(modelg=mx_model,   pop = 0.1 + 0.9*poplist_gpu, x_low=x_l, x_hig=x_h)
            
            if i <= 0:
                bests_gpu = np.array(bests)
                best_arr = np.tile(np.reshape( np.tile(bests_gpu, (1,popsize)), (num_isles*popsize, num_layers_i) ), (ls_fac,1))
                a_arr = shifts + np.random.randint(0, popsize, ls_fac*num_isles*popsize)
                b_arr = shifts + np.random.randint(0, popsize, ls_fac*num_isles*popsize)
                c_arr = shifts + np.random.randint(0, popsize, ls_fac*num_isles*popsize)
                
                bestora = poplist_gpu[a_arr]
                bestora[bora >= 0.5] = best_arr[bora >= 0.5]
                
                mutpop = np.clip( bestora + mutrad*(poplist_gpu[b_arr] - poplist_gpu[c_arr]), 0, 1)
                mutpop = np.clip( bestora + mut*(poplist_gpu[b_arr] - poplist_gpu[c_arr]), 0, 1)
                crosspop = np.random.uniform( low=0.0, high=1.0, size=(ls_fac*num_isles*popsize, num_layers_i ))
                poplist_gpu_big = np.tile(poplist_gpu, (ls_fac,1))
                trial_big = np.where(crosspop > crp, mutpop, poplist_gpu_big)
                f_isl_gpu_big = fobj_mx(modelg=mx_model,   pop = 0.1 + 0.9*trial_big, x_low=x_l, x_hig=x_h)
                f_isl_mat = np.reshape(f_isl_gpu_big, (ls_fac, num_isles*popsize))
                f_isl_gpu = np.min(f_isl_mat, axis=0)
                indexes = num_isles*popsize*np.argmin(f_isl_mat, axis=0) + np.arange(num_isles*popsize)
                trial = trial_big[indexes]
                spl1 = np.split(trial, num_isles)
                trilist = []
                for isle in range(num_isles):
                    trilist.append( [x.flatten() for x in  np.split(spl1[isle], popsize)])
                tflat = list(chain.from_iterable(trilist))
            
            else:  
                for isln in range(num_isles):
                    for j in range(popsize):
                        idxs = [idx for idx in range(popsize) if idx != j]
                        picks = np.random.choice(idxs, 3, replace = False)
                        a, b, c = poplist[isln][picks[0]],  poplist[isln][picks[1]], poplist[isln][picks[2]]

                        if np.random.rand() < 0.0:
                            mutant = cre_mutant(a, b, c, mut)
                            trilist[isln][j] = crossov(mutant, poplist[isln][j], crossp)
                        else:
                            mutant = cre_mutant(bests[isln], b, c, mut)
                            trilist[isln][j] = crossov(mutant, poplist[isln][j], crossp)
                 
                tflat = list(chain.from_iterable(trilist))
                f_isl_gpu = fobj_mx(modelg=mx_model,   pop = 0.1 + 0.9*np.array(tflat), x_low=x_l, x_hig=x_h)
           
            do_calc = np.where( f_isl_gpu < fitness_isl_gpu, np.ones_like(fitness_isl_gpu), np.zeros_like(fitness_isl_gpu)).astype('int32')           
            #print(do_calc)
            dc = do_calc
            tflat_subl = [tflat[x] for x in np.argwhere(dc == 1).flatten()]
            
            
            f_isl = np.copy(fitness_isl)
            f_isl[dc == 1] = np.asarray(fobj(tflat_subl, **kwargs))
            f = np.split(f_isl, num_isles)
            num_func_evals+=len(tflat_subl)
            #print(len(tflat_subl))
            
            used_calc = np.where( f_isl < fitness_isl, np.ones_like(fitness_isl), np.zeros_like(fitness_isl)).astype('int32')           
            #print(used_calc)
            err_bits = (np.logical_xor(do_calc, used_calc)) + 0
            
            if(np.sum(err_bits)/np.sum(do_calc) >= 0.75 ):
                tflat_subl_n = [tflat[x] for x in np.argwhere(dc != 1).flatten()]
                f_isl[dc != 1] = np.asarray(fobj(tflat_subl_n, **kwargs))
                num_func_evals+=len(tflat_subl_n)
            errhist.append(np.sum(err_bits)/np.sum(do_calc))
            #print(np.sum(err_bits)/np.sum(do_calc))
            #print(err_bits)
            #print(" ")
            
            
            for isln in range(num_isles):
                for j in range(popsize):
                    if f[isln][j] < fitness[isln][j]:
                        fitness[isln][j] = f[isln][j]
                        poplist[isln][j] = trilist[isln][j]
                        
                        if f[isln][j] < fitness[isln][bids[isln]]:
                            bids[isln] = j
                            bests[isln] = trilist[isln][j]
            
                bfits[isln] = fitness[isln][bids[isln]]
            history.append(np.copy(bfits))
            
            
             
            if (i+1)%20 == 0:
               
                print("%3d" %i, end =" -- ")
                for num in bfits:
                    print("%4.3f" %num, end="  ")
                print(" ")
            
        
                
            
        if its > 64:
            its = int(its/2)
        
     

        if gen < (num_gens - 1):
            #print("Round robin best migration")
            stmp = np.copy(poplist[num_isles-1][bids[num_isles-1]])
            for isln in range(num_isles-1, 0, -1):
                
                poplist[isln][bids[isln]] = np.copy(poplist[isln-1][bids[isln-1]])
            poplist[0][bids[0]] = stmp 
            
    
    
    
    
    
    print("Num func evals: ", num_func_evals)
            
    return bids, bests, bfits, np.asarray(history), num_func_evals, np.asarray(errhist)
            



def de_isl(
    fobj,  # objective function, serial eval
    num_layers_i=16,
    num_isles = 5, #number of islands
    num_gens =5,
    poplist=[],  # already initialized population
    mut=0.8,  # mutation rate
    crossp=0.7,  # crossover
    lenp=0.08,
    lins=0.06,
    popsize=20,  # the population size
    its=1000, # the number of iterations needed to run for
    verbose=0,
    **kwargs
):
    """
        This function performs diff evolution using fobj evaluated in parallel  
        returns the  last pop, fitness of pop, best individual and opt history 
    """
    #mut = 0.8 + 0.6*np.random.random()
    history = []
    bids = np.zeros(num_isles, dtype=int)
    bfits = np.zeros(num_isles)
    mut_r = np.random.uniform(0.5, 1.5, num_isles)
    cro_r = np.random.uniform(0.3, 0.8, num_isles)
    num_func_evals = 0

    trilist = []
    for isl in range(num_isles):
        tmp = np.random.uniform(0,1, popsize*num_layers_i)
        trilist.append( np.split(tmp, popsize) ) 
    tmp2 = np.random.uniform(0,1, num_isles*num_layers_i)
    bests = np.split(tmp2, num_isles)
        
    
    
    

    for gen in range(num_gens):
        if verbose == 0:
            print("==============================")
            print("Epoch #:" + str(gen + 1))
            print("==============================")
        
        isl = list(chain.from_iterable(poplist))
        fitness_isl = np.asarray(fobj(isl, **kwargs))
        num_func_evals+=len(isl)
        fitness = np.split(fitness_isl, num_isles)
        
        for isln in range(num_isles):
            bids[isln] = np.argmin(fitness[isln])
            bests[isln] = poplist[isln][bids[isln]]
            
        for i in range(its):
            for isln in range(num_isles):
                for j in range(popsize):
                    idxs = [idx for idx in range(popsize) if idx != j]
                    picks = np.random.choice(idxs, 3, replace = False)
                    a, b, c = poplist[isln][picks[0]],  poplist[isln][picks[1]], poplist[isln][picks[2]]
                    
                    if np.random.rand() < 0.5:
                        mutant = cre_mutant(a, b, c, mut_r[isln])
                        child = crossov(mutant, poplist[isln][j], cro_r[isln])
                    
                    elif np.random.rand() < lenp:
                        best_n = lengthen_best(bests[isln])
                        mutant = cre_mutant(best_n, b, c, mut_r[isln])
                        child = crossov(mutant, poplist[isln][j], cro_r[isln])
                    else:
                        mutant = cre_mutant(bests[isln], b, c, mut_r[isln])
                        child = crossov(mutant, poplist[isln][j], cro_r[isln])
                        if np.random.rand() <=lins:
                            child = ins_best(child)
                    
                    trilist[isln][j] = child 
                    
            tflat = list(chain.from_iterable(trilist))
            f_isl = np.asarray(fobj(tflat, **kwargs))
            f = np.split(f_isl, num_isles)
            num_func_evals+=len(tflat)
            
            for isln in range(num_isles):
                for j in range(popsize):
                    if f[isln][j] < fitness[isln][j]:
                        fitness[isln][j] = f[isln][j]
                        poplist[isln][j] = trilist[isln][j]
                        if f[isln][j] < fitness[isln][bids[isln]]:
                            bids[isln] = j
                            bests[isln] = trilist[isln][j]
            
                bfits[isln] = fitness[isln][bids[isln]]
            

            if (i+1)%20 == 0:
                print("%3d" %i, end =" -- ")
                for num in bfits:
                    print("%4.3f" %num, end="  ")
                print()
            
            history.append(np.copy(bfits))
              
        
                
            
        if its > 64:
            its = int(its/2)
        
     

        if gen < (num_gens - 1):
            #print("Round robin best migration")
            stmp = np.copy(poplist[num_isles-1][bids[num_isles-1]])
            for isln in range(num_isles-1, 0, -1):
                
                poplist[isln][bids[isln]] = np.copy(poplist[isln-1][bids[isln-1]])
            poplist[0][bids[0]] = stmp 
            
    print("Num func evals: ", num_func_evals)
            
    return bids, bests, bfits, np.asarray(history), num_func_evals
            





