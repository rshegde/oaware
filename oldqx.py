#import snlay as sn
import numpy as np
from scipy import interpolate
#from noise import pnoise1
import random
from tmm import coh_tmm
import multiprocessing as mp
#import makeqx as mkq
#import pyximport; pyximport.install(pyimport=True)
#import mtmm as mtmm


# make a materials dictionary
matsdict = {
    1: "./materials/gold.dat",
    2: "./materials/silicon.dat",
    3: "./materials/silica.dat",
    4: "./materials/tio2.dat",
    5: "./materials/silver.dat",
}


def get_nk(datafile, wavelengths):
    """Reads the given file and returns the n+ik complex at
    the given wavelength after suitable interpolation
    :datafile: TODO
    :wavelength: TODO
    :returns: TODO
    """
    rawdisp = np.loadtxt(datafile)
    f_r = interpolate.interp1d(rawdisp[:, 0], rawdisp[:, 1])
    f_i = interpolate.interp1d(rawdisp[:, 0], rawdisp[:, 2])
    return f_r(wavelengths) + 1j * f_i(wavelengths)


def narr(qx_cum, xv):
    if (qx_cum[qx_cum <= xv].size + 1)%2 == 1:
        return 1.45 
    else:
        return 2.58

def give_n(qx):
    print(qx)
    qx_cum = np.cumsum(qx)
    print(qx_cum)
    x_vals = np.linspace(0,12.5, 3000)
    y_vals = np.ones_like(x_vals)
    yv_right = 12
    yv_left =  12 - qx_cum[-1]
    #print(yv_left)
    y_vals[x_vals > yv_right] = 1.52
    y_vals[x_vals < yv_left ] = 1.00
    tnew = np.array([narr(qx_cum, x- yv_left) for x in x_vals[np.logical_and( x_vals > yv_left, x_vals < yv_right)] ])
    y_vals[np.logical_and( x_vals > yv_left, x_vals < yv_right)] = tnew 
    return x_vals, y_vals 


def nk_2_eps(n):
    """TODO: Docstring for nk_2_epsnk_2_eps.
    :returns: complex epsilon given n and kappa 
    """
    eps_r = n.real ** 2 - n.imag ** 2
    eps_i = 2 * n.real * n.imag
    return eps_r + 1j * eps_i


def eps_2_nk(eps):
    """TODO: Docstring for nk_2_epsnk_2_eps.
    :returns: complex epsilon given n and kappa 
    """
    modeps = np.abs(eps)
    n_r = np.sqrt(0.5 * (modeps + eps.real))
    n_i = np.sqrt(0.5 * (modeps - eps.real))
    return n_r + 1j * n_i


def LL_mixing(fH, n_H, n_L):
    """TODO: Docstring for brugg_mixingbrugg_mixing.
    Given the volumne fraction of the higher index material, give the effective
    index of the layer
    :fH: volumne fraction from 0 to 1 
    :n_H: ri  of the higher index material 
    :n_L: ri of the lower index material 
    :returns: TODO
    """
    eH = nk_2_eps(n_H)
    eL = nk_2_eps(n_L)
    bigK = fH * (eH - 1) / (eH + 2) + (1 - fH) * (eL - 1) / (eL + 2)
    e_eff = (1 + 2 * bigK) / (1 - bigK)
    return eps_2_nk(e_eff)


def make_nxdx(qx, wavelen=550, n_substrate=1.52):
    """TODO: Docstring for make_nxdx.
    :qx: TODO
    :n_substrate: TODO
    :layer_thickness: TODO
    :returns: TODO
    """
    num_layers = int(qx.size)
#     qx = 10 + 90*qx
   
    d_x = [np.inf] + qx.tolist() + [np.inf]
    sde = np.zeros_like(qx) 
#     sde[::2] = get_nk(datafile=matsdict[3], wavelengths=wavelen) #1.45
#     sde[1::2] = get_nk(datafile=matsdict[4], wavelengths=wavelen) #2.58
    sde[::2] = 1.45 #get_nk(datafile=matsdict[3], wavelengths=wavelen) #1.45
    sde[1::2] = 2.58 #get_nk(datafile=matsdict[4], wavelengths=wavelen) #2.58
    n_x = [1.0] + sde.tolist() + [n_substrate]
    
    return d_x, n_x


def tmm_eval_wsweep(
    qx, inc_ang, lam_low=400, lam_high=800, lam_pts=64, n_subs=1.45  # in nm
):
    degree = np.pi / 180
    lam_inv = np.linspace(1/lam_low, 1/lam_high, num=lam_pts, endpoint=True)
    lams = 1.0/lam_inv
   
    Rs = np.zeros(lams.size)
    # Rp = np.zeros(lams.size)
    for lidx, lam in enumerate(lams):
        d_x, n_x = make_nxdx(qx=qx, wavelen=lam, n_substrate=n_subs)
        Rs[lidx] = coh_tmm("s", n_x, d_x, th_0=inc_ang * degree, lam_vac=lam)['R']
        # Rp[lidx] = 100*coh_tmm('p',n_x,d_x, th_0=inc_ang*degree,lam_vac=lam)
    return Rs


def grating_r(L, K, n_avg, eps_m, lams):
    R = lams - lams + 1j
    
    for lidx, lam in enumerate(lams):
        k0 = 2*np.pi/lam
        k = 2*np.pi*n_avg/lam
        #K = 2*np.pi/P
        delb = 2*k - K
        Kappa = 0.5*k0*eps_m/n_avg
        s = np.sqrt(Kappa*np.conj(Kappa) - (delb/2)**2)
        
        term1 = -1j*np.conj(Kappa)*np.sinh(s*L)
        term2 = s*np.cosh(s*L)
        term3 = 1j*(delb/2.0)*np.sinh(s*L)
    
        R[lidx] = term1/(term2+term3)
    
    return R




def copmodtheory(
    qx, inc_ang, lam_low=400, lam_high=800, lam_pts=64, n_subs=1.52  # in nm
):
    degree = np.pi / 180
    lam_inv = np.linspace(1/lam_low, 1/lam_high, num=lam_pts, endpoint=True)
    lams = 1.0/lam_inv
   
    Rs = np.zeros(lams.size)
    Rs = Rs + 1j*0.0
    
    #for lidx, lam in enumerate(lams):
    d_x, n_x = make_nxdx(qx=qx, wavelen=400, n_substrate=n_subs)
    nx=np.asarray(n_x)[1:-1].flatten()
    epsx = nx**2
    dx=np.asarray(d_x)[1:-1].flatten()
    n_avg = np.sqrt(np.sum(epsx*dx)/np.sum(dx))
    L = np.sum(dx)

    epsx = np.append(epsx - n_avg**2, 0.0)
    #k_avg = 2.0*np.pi/(lam/n_avg)

    dz = 1
    a = np.arange(0,L,dz)

    freqs = np.arange(0,1.0/dz, dz/(L))
    stack = np.array(dx)
    sc = np.cumsum(stack) - dz
    indexes = np.searchsorted(sc, a)
    ri = epsx[indexes]
    eps_om = np.fft.fft(ri)/L
    for ind in np.arange(1,15,1):
        Rs = Rs + grating_r(L,2*np.pi*freqs[ind], n_avg, eps_om[ind], lams)
    
    Rc1 = (1.0 - 1.45)/ (2.45)
    Rs = Rs + Rc1/ (1 + Rc1*Rs)
    Rs = (np.abs(Rs))**2
    Rs = Rs/np.amax(Rs)
    
    return Rs
   


def cpt(
    qx, inc_ang, lam_low=400, lam_high=800, lam_pts=64, n_subs=1.52  # in nm
):
    degree = np.pi / 180
    lam_inv = np.linspace(1/lam_low, 1/lam_high, num=lam_pts, endpoint=True)
    lams = 1.0/lam_inv
   
    Rs = np.zeros(lams.size)
    Rs = Rs + 1j*0.0
    
    for lidx, lam in enumerate(lams):
        d_x, n_x = make_nxdx(qx=qx, wavelen=lam, n_substrate=n_subs)
        nx=np.asarray(n_x)[1:-1].flatten()
        dx=np.asarray(d_x)[1:-1].flatten()
        n_avg = np.sum(nx*dx)/np.sum(dx)
        L = np.sum(dx)
        
        
        
        nx = np.append(nx - n_avg, 0.0)
        k_avg = 2.0*np.pi/(lam/n_avg)
        
        
        dz = 0.1
        a = np.arange(0,L + dz,dz)
        
        freqs = np.arange(0,0.5/dz, dz/(L+dz))
        stack = np.array(dx)
        sc = np.cumsum(stack)
        indexes = np.searchsorted(sc, a)
        ri = nx[indexes]
        
        
#         fftri = np.fft.fft(ri)/D
#         lam_pm_idx = int(2*D*n_avg/lam)
        
        
#         #
#         kapL = 24*0.5*L*k_avg*(fftri[lam_pm_idx])**2
#         r = -1j*np.conj(kapL)*np.tanh(kapL)/kapL
#         t = 1.0/np.cosh(kapL)
#         print(t)
        
        
#         # interface r and t
#         nc = 1.0 # cladding
#         ns = 1.52  # substrate 
#         r12 = (nc - n_avg)/(nc + n_avg)
#         r23 = (n_avg - ns)/(ns + n_avg)
        
        
#         t = t*np.exp(-1j*k_avg*2.0*L)
#         Rs[lidx] = np.abs( (r12 + r23*t*t)/(1.0 + t*t*r12*r23) )**2
        
        
        
        
        
    return ri










# def tmm_eval_wsweep_m(
#     qx, inc_ang, lam_low=400, lam_high=800, lam_pts=256, n_subs=1.52  # in nm
# ):
#     degree = np.pi / 180
#     lam_inv = np.linspace(1/lam_low, 1/lam_high, num=lam_pts, endpoint=True)
#     lams = 1.0/lam_inv
#     Rs = np.zeros(lams.size)
#     # Rp = np.zeros(lams.size)
#     for lidx, lam in enumerate(lams):
#         d_x, n_x = make_nxdx(qx=qx, wavelen=lam, n_substrate=n_subs)
#         Rs[lidx] = 100 * coh_tmm("s", n_x, d_x, th_0=inc_ang * degree, lam_vac=lam)
#         # Rp[lidx] = 100*coh_tmm('p',n_x,d_x, th_0=inc_ang*degree,lam_vac=lam)
#     return np.mean(Rs)

