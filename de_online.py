import numpy as np
import random
import mxnet.ndarray as nd
import mxnet as mx

from itertools import chain



#test for exact equality
def arreq_in_list(myarr, list_arrays):
    return next((True for elem in list_arrays if np.array_equal(elem, myarr)), False)


def lengthen_best(best):
    return np.append(best, np.random.rand())

def ins_best(best):
    ins_loc = np.random.choice(best.size)
    return np.insert(best, (ins_loc, ins_loc), np.random.uniform(0,1,2))

def pad_chrom(inp, max_len):
    if inp.size < max_len:
        #ins_loc = np.random.choice(inp.size, max_len - inp.size, replace=False)
        
        ins_val = np.zeros(max_len - inp.size)
        #ins_val = np.random.uniform(0,1, max_len - inp.size)
        return np.append(inp, ins_val)
    else:
        return inp


def cre_mutant(best, b, c, mut):
#     max_len = max( best.size, b.size, c.size)
#     best_n = pad_chrom(best, max_len=max_len)
#     b_n = pad_chrom(b, max_len=max_len)
#     c_n = pad_chrom(c, max_len=max_len)
    #return np.clip(best_n + mut*(b_n - c_n), 0, 1)
    return np.clip(best + mut*(b - c), 0, 1)



def crossov(mutant, curr, crossp):
#     max_len = max( curr.size, mutant.size)
#     if curr.size == mutant.size:
#         c_r = curr
#         m_r = mutant
#     else:
#         c_r = pad_chrom(curr, max_len)
#         m_r = pad_chrom(mutant, max_len)
    
    cross_points = np.random.rand(curr.size) > crossp
#     if not np.any(cross_points):
#         cross_points[np.random.randint(0, curr.size)] = True
    #return np.where(cross_points, m_r, c_r)
    return np.where(cross_points, mutant, curr)


def de_plain(
    fobj,  # objective function, calculates spectrum and then evaluates target function
    fsec,  # this is the exact spectrum calcculation tool
    xfname,
    yfname,
    num_layers_i=16,
    num_isles = 1, #number of islands
    num_gens =1,
    poplist=[],  # already initialized population
    mut=0.8,  # mutation rate
    crossp=0.7,  # crossover
    lenp=0.08,
    lins=0.06,
    popsize=20,  # the population size
    its=1000, # the number of iterations needed to run for
    verbose=0,
    **kwargs
):
    """
        This function performs diff evolution using fobj evaluated in parallel  
        returns the  last pop, fitness of pop, best individual and opt history 
    """
    rawx = open(xfname+".dat", "a") # or "wb"
    rawy = open(yfname+".dat", "a") # or "wb"
    train_x_list = []
    train_y_list =[]
    #mut = 0.8 + 0.6*np.random.random()
    history = []
    bids = np.zeros(num_isles, dtype=int)
    bfits = np.zeros(num_isles)
    mut_r = np.random.uniform(0.5, 1.5, num_isles)
    cro_r = np.random.uniform(0.3, 0.8, num_isles)
    num_func_evals = 0

    trilist = []
    for isl in range(num_isles):
        tmp = np.random.uniform(0,1, popsize*num_layers_i)
        trilist.append( np.split(tmp, popsize) ) 
    tmp2 = np.random.uniform(0,1, num_isles*num_layers_i)
    bests = np.split(tmp2, num_isles)
        
    
    
    

    for gen in range(num_gens):
        if verbose == 0:
            print("==============================")
            print("Epoch #:" + str(gen + 1))
            print("==============================")
        
        isl = list(chain.from_iterable(poplist))
        fitness_isl = np.asarray(fobj(isl, **kwargs))
        num_func_evals+=len(isl)
        fitness = np.split(fitness_isl, num_isles)
        
        for isln in range(num_isles):
            bids[isln] = np.argmin(fitness[isln])
            bests[isln] = poplist[isln][bids[isln]]
            
        for i in range(its):
            for isln in range(num_isles):
                for j in range(popsize):
                    idxs = [idx for idx in range(popsize) if idx != j]
                    picks = np.random.choice(idxs, 3, replace = False)
                    a, b, c = poplist[isln][picks[0]],  poplist[isln][picks[1]], poplist[isln][picks[2]]
                    
                    if np.random.rand() < 0.5:
                        mutant = cre_mutant(a, b, c, mut_r[isln])
                        child = crossov(mutant, poplist[isln][j], cro_r[isln])
                    
                    elif np.random.rand() < lenp:
                        best_n = lengthen_best(bests[isln])
                        mutant = cre_mutant(best_n, b, c, mut_r[isln])
                        child = crossov(mutant, poplist[isln][j], cro_r[isln])
                    else:
                        mutant = cre_mutant(bests[isln], b, c, mut_r[isln])
                        child = crossov(mutant, poplist[isln][j], cro_r[isln])
                        if np.random.rand() <=lins:
                            child = ins_best(child)
                    
                    trilist[isln][j] = child 
                    
            
            tflat = list(chain.from_iterable(trilist))
            mikspec = fsec(tflat, **kwargs)
            #print(mikspec.shape)
            f_isl = 100*np.mean(mikspec, axis=1)
            #f_isl = np.asarray(fobj(tflat, **kwargs))
            f = np.split(f_isl, num_isles)
            
            train_x_list = train_x_list + tflat #  [tflat[x] for x in np.argwhere(err_bits == 1).flatten()]
            #train_x_app = np.asarray([tflat[x] for x in np.argwhere(err_bits == 1).flatten()])
            train_y_list = train_y_list +  [mikspec[i] for i in range(len(tflat)) ] 
            
            
            
            num_func_evals+=len(tflat)
            
            
            
            for isln in range(num_isles):
                for j in range(popsize):
                    if f[isln][j] < fitness[isln][j]:
                        fitness[isln][j] = f[isln][j]
                        poplist[isln][j] = trilist[isln][j]
                        if f[isln][j] < fitness[isln][bids[isln]]:
                            bids[isln] = j
                            bests[isln] = trilist[isln][j]
            
                bfits[isln] = fitness[isln][bids[isln]]
            

            if (i+1)%1 == 0:
                print("%3d" %i, end =" -- ")
                for num in bfits:
                    print("%4.3f" %num, end="  ")
                print()
            
            history.append(np.copy(bfits))
              
        
                
            
        if its > 64:
            its = int(its/2)
        
     

        if gen < (num_gens - 1):
            #print("Round robin best migration")
            stmp = np.copy(poplist[num_isles-1][bids[num_isles-1]])
            for isln in range(num_isles-1, 0, -1):
                
                poplist[isln][bids[isln]] = np.copy(poplist[isln-1][bids[isln-1]])
            poplist[0][bids[0]] = stmp 
            
    print("Num func evals: ", num_func_evals)
    train_x_list = np.asarray(train_x_list)
    train_y_list = np.asarray(train_y_list)
    np.savetxt(rawx, train_x_list)
    np.savetxt(rawy, train_y_list)
    rawx.close()
    rawy.close()
            
    return bids, bests, bfits, np.asarray(history), num_func_evals
            


def de_isl_online(
    fobj,  # objective function which gives spectra, internally converted to fitness
    fobj_mx,
    fsec, 
    xfname,
    yfname,
    mx_model=[], 
    x_l= 0.1,
    x_h = 1.0,
    num_layers_i=16,
    num_isles = 1, #number of islands
    num_gens =1,
    poplist=[],  # already initialized population
    mut=0.8,  # mutation rate
    crossp=0.7,  # crossover
    lenp=0.08,
    lins=0.06,
    popsize=20,  # the population size
    its=1000, # the number of iterations needed to run for
    verbose=0,
    **kwargs
):
    """
        This function performs diff evolution using fobj evaluated in parallel  
        returns the  last pop, fitness of pop, best individual and opt history 
    """
    rawx = []
    rawy = []
        
    bucketsz = 0 
    history = []
    errhist =[]
    train_x_list = [ [] for isle in range(num_isles) ]
    train_y_list = [ [] for isle in range(num_isles) ]
    #mut = 0.8 + 0.6*np.random.random()
    bids = np.zeros(num_isles, dtype=int)
    bfits = np.zeros(num_isles)
    #mut_r = np.random.uniform(0.8, 0.8, num_isles)
    #cro_r = np.random.uniform(1.0, 1.0, num_isles)
    num_func_evals = 0
    use_gpu = 1
    
    
    trilist = []
    for isl in range(num_isles):
        tmp = np.random.uniform(0,1, popsize*num_layers_i)
        trilist.append( np.split(tmp, popsize) ) 
    tmp2 = np.random.uniform(0,1, num_isles*num_layers_i)
    bests = np.split(tmp2, num_isles)

    
    
    
    for gen in range(num_gens):
        if verbose == 0:
            print("==============================")
            print("Epoch #:" + str(gen + 1))
            print("==============================")
        
        isl = list(chain.from_iterable(poplist))
        fitness_isl = np.asarray(fobj(isl, **kwargs))
        
        num_func_evals+=len(isl)
        fitness = np.split(fitness_isl, num_isles)
        
        for isln in range(num_isles):
            bids[isln] = np.argmin(fitness[isln])
            bests[isln] = poplist[isln][bids[isln]]
            
        for i in range(its):
            isl = list(chain.from_iterable(poplist))
            
            poplist_gpu = np.array(isl) 
           
            fitness_isl_gpu = fitness_isl - fitness_isl 
            for isle in range(num_isles):
                fitness_isl_gpu[(isle)*popsize:(isle+1)*popsize] = fobj_mx(modelg=mx_model[isle],   pop = 0.1 + 0.9*poplist_gpu           [(isle)*popsize:(isle+1)*popsize], x_low=x_l, x_hig=x_h)
            
            
            
            for isln in range(num_isles):
                for j in range(popsize):
                    idxs = [idx for idx in range(popsize) if idx != j]
                    picks = np.random.choice(idxs, 3, replace = False)
                    a, b, c = poplist[isln][picks[0]],  poplist[isln][picks[1]], poplist[isln][picks[2]]
                    mutant = cre_mutant(bests[isln], b, c, mut)
                    trilist[isln][j] = crossov(mutant, poplist[isln][j], crossp)
                    

                   
            tflat = list(chain.from_iterable(trilist))
            
            
            
            f_isl_gpu = fitness_isl - fitness_isl   #fobj_mx(modelg=mx_model,   pop = 0.1 + 0.9*np.array(tflat), x_low=x_l, x_hig=x_h)
            for isle in range(num_isles):
                f_isl_gpu[(isle)*popsize:(isle+1)*popsize] = fobj_mx(modelg=mx_model[isle],   pop = 0.1 + 0.9*np.array(tflat)           [(isle)*popsize:(isle+1)*popsize], x_low=x_l, x_hig=x_h)
           
            f_isl = np.copy(fitness_isl)
            
            for isle in range(num_isles):
                gslice = f_isl_gpu[(isle)*popsize:(isle+1)*popsize]
                gitslice = fitness_isl_gpu[(isle)*popsize:(isle+1)*popsize]
                fslice = f_isl[(isle)*popsize:(isle+1)*popsize]
                fitslice = fitness_isl[(isle)*popsize:(isle+1)*popsize]
                dc = np.where( gslice < gitslice, np.ones_like(gslice), np.zeros_like(gitslice)).astype('int32')           
                
                tflat_c = tflat[(isle)*popsize:(isle+1)*popsize]
                tflat_subl = [tflat_c[x] for x in np.argwhere(dc == 1).flatten()]
                
                fcalls=0
                 
                if len(tflat_subl) > 0:
                    mikspec = fsec(tflat_subl, **kwargs)
                    fslice[dc == 1] = 100*np.mean(mikspec, axis=1)
                    num_func_evals+=len(tflat_subl)
                    fcalls = len(tflat_subl)
                    train_x_list[isle] = train_x_list[isle] +  tflat_subl
                    train_y_list[isle] = train_y_list[isle] +  [mikspec[i] for i in range(len(tflat_subl)) ]
                
                uc = np.where( fslice < fitslice, np.ones_like(fslice), np.zeros_like(fitslice)).astype('int32')           
                err_bits = (np.logical_xor(dc, uc)) + 0
                
                default_step = 0
                if (len(tflat_subl) == 0):
                    default_step = 1
                else:
                    default_step = np.sum(err_bits)/np.sum(dc)
                    #print(default_step)
                
                if(default_step >= 0.75):
                    tflat_subl_n = [tflat_c[x] for x in np.argwhere(dc != 1).flatten()]
                    if len(tflat_subl_n) > 0:
                        mikspec2 = fsec(tflat_subl_n, **kwargs)
                        #print(mikspec.shape)
                        fslice[dc != 1] = 100*np.mean(mikspec2, axis=1)
                        num_func_evals+=len(tflat_subl_n)
                        fcalls+=len(tflat_subl_n)
                        train_x_list[isle] = train_x_list[isle] + tflat_subl_n #  [tflat[x] for x in np.argwhere(err_bits == 1).flatten()]
                        train_y_list[isle] = train_y_list[isle] +  [mikspec2[i] for i in range(len(tflat_subl_n)) ]
                #errhist.append(np.sum(err_bits)/np.sum(do_calc))
                #print(np.sum(err_bits)/np.sum(do_calc))
                #print(err_bits)
                #print(" ")
            
            f = np.split(f_isl, num_isles)
            print("Num func evals: ", fcalls)
            
            for isln in range(num_isles):
                for j in range(popsize):
                    if f[isln][j] < fitness[isln][j]:
                        fitness[isln][j] = f[isln][j]
                        poplist[isln][j] = trilist[isln][j]
                        
                        if f[isln][j] < fitness[isln][bids[isln]]:
                            bids[isln] = j
                            bests[isln] = trilist[isln][j]
            
                bfits[isln] = fitness[isln][bids[isln]]
            history.append(np.copy(bfits))
            
            
             
            if (i+1)%1 == 0:
               
                print("%3d" %i, end =" -- ")
                for num in bfits:
                    print("%4.3f" %num, end="  ")
                print(" ")
        
     

        if gen < (num_gens - 1):
            #print("Round robin best migration")
            stmp = np.copy(poplist[num_isles-1][bids[num_isles-1]])
            for isln in range(num_isles-1, 0, -1):
                
                poplist[isln][bids[isln]] = np.copy(poplist[isln-1][bids[isln-1]])
            poplist[0][bids[0]] = stmp 
            
    print("Num func evals: ", num_func_evals)
    
    for isl in range(num_isles):
        namef = xfname+str(isl)+".dat"
        rawx = open(namef, "a") # or "wb"
        np.savetxt(rawx, np.asarray(train_x_list[isl]))
        rawx.close()
        namef = yfname+str(isl)+".dat"
        rawy = open(namef, "a") # or "wb"
        np.savetxt(rawy, np.asarray(train_y_list[isl]))
        rawy.close()
    
    
    
    return bids, bests, bfits, np.asarray(history), num_func_evals, np.asarray(errhist)
            




